# -*- coding: utf-8 -*-
"""
@author: Lucas Zane
"""

import pandas as pd
import numpy as np
import pyodbc
from db_credentials import sqlserver_db_config
from insert_queries import *
 
cnxn = pyodbc.connect(sqlserver_db_config)

def connect():
    cnxn = pyodbc.connect(sqlserver_db_config)
    cnxn.timeout = 0
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    return cursor


def ExecuteQuery(cursor, data, query, fieldNames):
    data = data.drop_duplicates()
    try:
        cursor.executemany(query, (([index, *row[fieldNames]]) for index,row in data.iterrows()))
    except:
        cnxn.commit()
        cursor = connect()
        cursor.executemany(query, (([index, *row[fieldNames]]) for index,row in data.iterrows()))


def ExecuteLongQuery(cursor, data, query, fieldNames):
    data = data.drop_duplicates()
    cursor.executemany(query, (([index, *row[fieldNames]]) for index,row in data.iterrows()))
        
def InsertBairros(cursor):
    data = pd.read_csv("data/bairros.csv", index_col="codigo")
    fieldNames = ['nome', 'municipio', 'uf', 'area']
    ExecuteQuery(cursor, data, bairroInsert, fieldNames)
    
def UpdatePopulacao(cursor):
    data = pd.read_json("data/populacao.json")
    data.set_index('codigo',inplace=True)
    data = data.dropna()
    fieldNames = ['populacao']
    ExecuteQuery(cursor, data, populacaoUpdate, fieldNames)
        
def InsertConcorrentes(cursor):
    data = pd.read_csv("data/concorrentes.csv", index_col="codigo", dtype={'codigo_bairro': str})
    fieldNames = ['nome','categoria','faixa_preco','endereco','municipio','uf','codigo_bairro']
    data = data.where(pd.notnull(data), '0')
    ExecuteQuery(cursor, data, concorrenteInsert, fieldNames)
    
def InsertEventoFluxo(cursor):
    data = pd.read_csv("data/eventos_de_fluxo.csv.gz",compression='gzip', index_col="codigo")
    fieldNames = ['datetime','codigo_concorrente']
    ExecuteLongQuery(cursor, data, eventoFluxoInsert, fieldNames)
            
def main():
    cnxn.timeout = 0
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    cnxn.autocommit = False
    
    """InsertBairros(cursor)
    print('Fim bairros')
    UpdatePopulacao(cursor)
    print('Fim populacao')
    InsertConcorrentes(cursor)
    print('Fim Concorrentes')"""
    InsertEventoFluxo(cursor)
    print('Fim EventoFluxo')
   
    cnxn.commit()
    cursor.close()
    cnxn.close()

if __name__ == "__main__":
    main()   
