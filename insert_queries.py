# -*- coding: utf-8 -*-

bairroInsert = """INSERT INTO dbo.Bairros([Codigo], [Nome], [Municipio], [UF], [Area]) 
    values(?,?,?,?,?)"""

populacaoUpdate = """
    DECLARE @cod_bairro varchar(30) = ?;
    DECLARE @qHab int = ?;
    
    UPDATE dbo.Bairros SET [QuantHabitantes]=@qHab
    WHERE [Codigo]=@cod_bairro
    """

concorrenteInsert = """
    DECLARE @codigo varchar(60)  = ?;
    DECLARE @nome varchar(60)  = ?;
    DECLARE @categoria varchar(60)  = ?;
    DECLARE @faixa int = ?;
    DECLARE @endereco varchar(60)  = ?;
    DECLARE @municipio varchar(60) = ?;
    DECLARE @uf varchar(4) = ?;
    DECLARE @cod_bairro varchar(30) = ?;
    
    IF @cod_bairro = '0' 
    	BEGIN SET @cod_bairro = NULL 
    	PRINT @cod_bairro END; 
    
    BEGIN INSERT INTO dbo.Concorrentes([Codigo],[Nome],[Categoria],[FaixaPreco],[Endereco],[Municipio],[UF],[CodBairro]) 
    values(@codigo ,@nome,@categoria,@faixa,@endereco,@municipio,@uf,@cod_bairro) END"""

eventoFluxoInsert = """
    INSERT INTO [dbo].[EventosFluxo]([Codigo], [DataEvento], [CodConcorrente]) 
        values(?,?,?)"""